fetch('noOfMatchesInYear.json').then(function(response) {
    return response.json();
}).then(function(json) {

    Highcharts.chart('plots1', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of matches played per year for all the years in IPL'
        },
        xAxis: {
            categories: json.map((element)=>element.season)
        },
        yAxis: {
            title: {
                text: 'Number of matches played'
            }
        },
        series:[{
            name : 'Number of matches played',
            data : json.map((element)=>element["number_of_matches"])
        }],   
    });

  }).catch(function(err) {
    console.log('Fetch problem: ' + err.message);
  });


  fetch('noOfMatchesWonPerTeamPerYear.json').then(function(response) {
    return response.json();
}).then(function(json) {

    let jsonobj = json.reduce((acc,cv)=>{
        if(acc[cv.winner] === undefined){
            acc[cv.winner] = {}
        }
        acc[cv.winner][cv.season]=cv.no_of_matches
        return acc
    },{})

    let year = [2008,2009,2010,2011,2012,2013,2014,2015,2016,2017]
    for(const [key, value] of Object.entries(jsonobj)){
        for(i in year){
            if(jsonobj[key][year[i]] === undefined){
                jsonobj[key][year[i]] = 0
            }
        }
    }

    for(const [key, value] of Object.entries(jsonobj)){
        jsonobj[key] = Object.values(value)
    }
  
    let values = []
    for(const [key, value] of Object.entries(jsonobj)){
        values.push({'name':key,'data':value})
    }

        Highcharts.chart('plots2', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of matches won per team per year in IPL.'
            },
            xAxis: {
                categories: year,
                title: {
                    text: 'Year'
                }
            },
            credits: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: 'Number of matches won'
                }
            },
            series: values,    
        });

  }).catch(function(err) {
    console.log('Fetch problem: ' + err.message);
  });

fetch('extraRunsConcededPerTeamInTheYear2016.json').then(function(response) {
    return response.json();
  }).then(function(json) {

        Highcharts.chart('plots3', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Extra Runs ConcededPer Team In The Year 2016'
            },
            xAxis: {
                categories: json.map((element)=>element.team)
            },
            yAxis: {
                title: {
                    text: 'Extra Runs'
                }
            },
            series:[{
                name : 'Extra Runs',
                data : json.map((element)=>element.extra_runs)
            }] ,    
        });

  }).catch(function(err) {
    console.log('Fetch problem: ' + err.message);
  });
  

  fetch('top10EconomicalBowlersInThe2015.json').then(function(response) {
    return response.json();
  }).then(function(json) {
    
        Highcharts.chart('plots4', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 10 economical bowlers in the year 2015'
            },
            xAxis: {
                categories: json.map((element)=>element.bowler)
            },
            yAxis: {
                title: {
                    text: 'Economy'
                }
            },
            series:[{
                name: 'Economy',
                data : json.map((element)=>element.economy)
            }] ,    
        });

  }).catch(function(err) {
    console.log('Fetch problem: ' + err.message);
  });
