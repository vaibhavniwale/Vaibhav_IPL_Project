let dotenv = require('dotenv');
let mysql = require('mysql');
const {DataExtractorMatches,DataExtractorDeliveries} = require('./index.js')

dotenv.config();
console.log(process.env.host)
let con = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: "mydb"
});

Promise.all([
    DataExtractorMatches(),
    DataExtractorDeliveries()
])
    .then(([
        matchesdata,
        deliveriesdata
    ])=>{
        csvtosql(matchesdata,deliveriesdata)}
    )
    .catch((err)=>{
        console.log(err)
    })

function csvtosql(matchesdata,deliveriesdata){
    return new Promise((resolve, reject) => {
        
        let matchesvalues = matchesdata.reduce((acc,cv)=>{
                
                let values = Object.values(cv).reduce((acc2,cv2)=>{
                    
                    if(!isNaN(cv2) && cv2.length !== 0){
                        acc2.push(Number(cv2))
                    }else{
                        acc2.push(cv2)
                    }
                    return acc2
                
                },[])
                acc.push(values)
                return acc
            },[])

        let Deliverisvalues = deliveriesdata.reduce((acc,cv)=>{

                let values = Object.values(cv).reduce((acc2,cv2)=>{
            
                    if(!isNaN(cv2) && cv2.length !== 0){
                        acc2.push(Number(cv2))
                    }else{
                        acc2.push(cv2)
                    }
                    return acc2
            
                },[])
                acc.push(values)
                return acc
            },[])

        try{
            con.connect(function(err) {

                if (err) reject(err);
                console.log("Connected!");
                
                let sql = "CREATE TABLE matches (id int,season int,city varchar(30),date date,team1 varchar(50),team2 varchar(50),toss_winner varchar(50),toss_decision varchar(10),result varchar(10),dl_applied int,winner varchar(50),win_by_runs int,win_by_wickets int,player_of_match varchar(30),venue varchar(70),umpire1 varchar(30),umpire2 varchar(30),umpire3 varchar(30))";
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("Table created");
                });
                
                sql = "INSERT INTO matches (id,season,city,date,team1,team2,toss_winner,toss_decision,result,dl_applied,winner,win_by_runs,win_by_wickets,player_of_match,venue,umpire1,umpire2,umpire3) VALUES ?";
                con.query(sql, [matchesvalues], function (err, result) {
                    if (err) throw err;
                    console.log("Number of records inserted: " + result.affectedRows);
                    });
                console.log("Connected!");
                
                sql = "CREATE TABLE deliveries (match_id int,inning int,batting_team varchar(35),bowling_team varchar(35),over_no int,ball int,batsman varchar(35),non_striker varchar(35),bowler varchar(35),is_super_over int,wide_runs int,bye_runs int,legbye_runs int,noball_runs int,penalty_runs int,batsman_runs int,extra_runs int,total_runs int,player_dismissed varchar(35),dismissal_kind varchar(35),fielder varchar(35))";
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    console.log("Table created");
                });
                
                sql = "INSERT INTO deliveries (match_id,inning,batting_team,bowling_team,over_no,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder) VALUES ?";
                con.query(sql, [Deliverisvalues], function (err, result) {
                    if (err) throw err;
                    console.log("Number of records inserted: " + result.affectedRows);
                });
                con.end();

            })

        } catch(err){
            reject(err)
        }
        
   })
}
