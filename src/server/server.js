const fs = require('fs')
const http = require('http')
const path = require('path')

//console.log(dirname)
http.createServer((request, response)=>{
    const reqUrlFileExt = request.url.split('.').splice(-1)[0]
    switch(reqUrlFileExt){
        case '/':
            const htmlFilePath = path.join(__dirname,'../client/index.html')
            //console.log(htmlFilePath)
            fs.readFile(htmlFilePath,'utf-8',(err, htmlData)=>{
                if(err){
                    response.writeHead(404)
                    console.log(err)
                } else{
                    response.writeHead(200,{
                        'Content-Type': "text/html"
                    })
                    response.end(htmlData)
                }
            })
            break;
        case 'js':
            const jsFilePath = path.join(__dirname,'../client/plot.js')
            //console.log(jsFilePath)
            fs.readFile(jsFilePath,'utf-8',(err, jsData)=>{
                if(err){
                    response.writeHead(404)
                    console.log(err)
                } else{
                    response.writeHead(200,{
                        'Content-Type': "application/javascript"
                    })
                    response.end(jsData)
                }
            })
            break
        case 'json':
            const jsonFilePath = path.join(__dirname,'../public/output'+request.url)
            
            //console.log(jsonFilePath)
            fs.readFile(jsonFilePath,'utf-8',(err, jsonData)=>{
                if(err){
                    response.writeHead(404)
                    console.log(err)
                } else{
                    response.writeHead(200,{
                        'Content-Type': "application/json"
                    })
                    //console.log(jsonData)
                    response.end(jsonData)
                }
            })
            break
        
    }
    //console.log('abc')
}).listen(3000);
