let dotenv = require('dotenv');
let mysql = require('mysql');

dotenv.config();
let con = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: "mydb"
});

function noOfMatchesInYear(){
    return new Promise((resolve, reject) => {
        con.connect(function(err) {
          if (err){
            reject(err)
          }else{
            con.query("select season,count(*) as number_of_matches from matches group by season order by season;", function (err, result, fields) {
              if (err){
                reject(err)
              }else{
                resolve(result);
              }
            });
          }
          con.end();
        });
    })
}
module.exports = {noOfMatchesInYear}