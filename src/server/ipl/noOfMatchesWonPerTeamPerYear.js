let dotenv = require('dotenv');
let mysql = require('mysql');

dotenv.config();
console.log(process.env.host)
let con = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: "mydb"
});

function noOfMatchesWonPerTeamPerYear(){
    return new Promise((resolve, reject) => {
        con.connect(function(err) {
          if (err){
            reject(err)
          }else{
            con.query("select season, winner,count(*) as no_of_matches from matches where result = 'normal' group by season, winner order by season asc;", function (err, result, fields) {
              if (err){
                reject(err)
              }else{
                resolve(result);
              }
            });
          }
          con.end();
        });
    })
}
module.exports = {noOfMatchesWonPerTeamPerYear}
