const dotenv = require('dotenv');
const mysql = require('mysql');

dotenv.config();
let con = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: "mydb"
});

function extraRunsConcededPerTeamInTheYear(year){
    return new Promise((resolve, reject) => {
        let filename = `extraRunsConcededPerTeamInTheYear${year}.json`
        con.connect(function(err) {
            if (err){
              reject(err)
            }else{
              con.query(`select batting_team as team, sum(extra_runs) as extra_runs from deliveries where match_id in (select id from matches where season = ${year}) group by batting_team order by batting_team;`, function (err, result, fields) {
                if (err){
                  reject(err)
                }else{
                  resolve([filename,result])
                }
              });
            }
            con.end();
          });
    })
}

module.exports = {extraRunsConcededPerTeamInTheYear}
