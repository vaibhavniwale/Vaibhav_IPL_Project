let dotenv = require('dotenv');
let mysql = require('mysql');

dotenv.config();
console.log(process.env.host)
let con = mysql.createConnection({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: "mydb"
});

function topNEconomicalBowlersInTheYear(year,num){
    return new Promise((resolve, reject) => {
        let filename = `top${num}EconomicalBowlersInThe${year}.json`
        con.connect(function(err) {
          if (err){
            reject(err)
          }else{
            con.query(`select bowler, (sum(total_runs)*6)/count(total_runs) as economy from deliveries where match_id in (select id from matches where season = ${year}) and bye_runs = 0 and legbye_runs = 0 group by bowler order by economy limit ${num};`, function (err, result, fields) {
              if (err){
                reject(err)
              }else{
                resolve([filename,result])
              }
            });
          }
          con.end();
        });
    })
}

module.exports = {topNEconomicalBowlersInTheYear}