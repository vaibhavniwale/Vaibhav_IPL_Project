const fs = require("fs");

const matchespath = './src/data/matches.csv'
const deliveriespath = './src/data/deliveries.csv'

const csv = require('csvtojson')

function DataExtractorMatches(){
     return new Promise((resolve, reject) => {
          csv()
               .fromFile(matchespath)
               .then((jsonObj)=>{
                    resolve(jsonObj)
               })
               .catch((err)=>{
                    reject(err)
               })
     })
}
function DataExtractorDeliveries(){
     return new Promise((resolve, reject) => {
          csv()
               .fromFile(deliveriespath)
               .then((jsonObj)=>{
                    resolve(jsonObj)
               })
               .catch((err)=>{
                    reject(err)
               })
     })
}
function JsonMaker(filename,obj){
     return new Promise((resolve, reject) => {
          fs.writeFile('./src/public/output/'+filename, JSON.stringify(obj), (error) => {
               reject(error);
          });
          resolve('Json file created')
     })
}
module.exports = {DataExtractorMatches,DataExtractorDeliveries,JsonMaker}
