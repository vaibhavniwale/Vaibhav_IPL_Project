const {noOfMatchesInYear} = require('../server/ipl/noOfMatchesInYear.js')
const {noOfMatchesWonPerTeamPerYear} = require('../server/ipl/noOfMatchesWonPerTeamPerYear.js')
const {extraRunsConcededPerTeamInTheYear} = require('../server/ipl/extraRunsConcededPerTeamInTheYear.js')
const {topNEconomicalBowlersInTheYear} = require('../server/ipl/topNEconomicalBowlersInTheYear.js')
const {DataExtractorMatches,DataExtractorDeliveries,JsonMaker} = require('../server/index.js')

noOfMatchesInYear()
    .then((obj)=>{
        console.log(JsonMaker('noOfMatchesInYear.json',obj))
    })
    .catch((err)=>console.log(err))

noOfMatchesWonPerTeamPerYear()
    .then((obj)=>{
        console.log(JsonMaker('noOfMatchesWonPerTeamPerYear.json',obj))
    })
    .catch((err)=>console.log(err))

extraRunsConcededPerTeamInTheYear(2016)
    .then(([filename,obj])=>{
        console.log(JsonMaker(filename,obj))
    })
    .catch((err)=>console.log(err))

topNEconomicalBowlersInTheYear(2015,10)
    .then(([filename,obj])=>{
        console.log(JsonMaker(filename,obj))
    })
    .catch((err)=>console.log(err))